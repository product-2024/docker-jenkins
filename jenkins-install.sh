#!/bin/sh
SCRIPT_DIR=$(cd $(dirname -- $0) && pwd)
IMAGE_FILE=jenkins-2.440.tar.gz

mkdir /var/opt/jenkins
cp -p ${SCRIPT_DIR}/data.tar.gz /var/opt/jenkins/.
cd /var/opt/jenkins/
tar xfz data.tar.gz && rm -f data.tar.gz

cd ${SCRIPT_DIR}
docker load -i ${IMAGE_FILE}
docker compose up -d
