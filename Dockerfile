FROM jenkins/jenkins:2.440-almalinux

LABEL y-ok <fairsky1985@gmail.com>

USER root

RUN dnf clean all
RUN dnf -y groupinstall "Development Tools"
RUN dnf -y install langpacks-ja
RUN cp /usr/share/zoneinfo/Asia/Tokyo /etc/localtime

ENV LANG="ja_JP.UTF-8"
ENV LANGUAGE="ja_JP:ja"
ENV LC_ALL="ja_JP.UTF-8"

ADD asset /asset

RUN rm -rf /opt/java
RUN rpm -Uvh --force /asset/jdk-11.0.20_linux-x64_bin.rpm
ENV JAVA_HOME=/usr/lib/jvm/jdk-11-oracle-x64
ENV PATH $PATH:$JAVA_HOME/bin

RUN tar xvzf /asset/apache-maven-3.9.6-bin.tar.gz -C /opt/
RUN ln -s /opt/apache-maven-3.9.6 /opt/apache-maven
RUN echo 'export PATH=$PATH:/opt/apache-maven/bin' | tee -a /etc/profile

COPY /asset/plugin.txt /usr/share/jenkins/ref/plugins.txt
RUN jenkins-plugin-cli --plugin-file /usr/share/jenkins/ref/plugins.txt

RUN dnf -y remove git
RUN dnf -y install curl-devel expat-devel gettext-devel openssl-devel zlib-devel perl-ExtUtils-MakeMaker
ADD /asset/git-2.43.0.tar.gz /usr/src/
WORKDIR /usr/src/git-2.43.0
RUN make configure
RUN ./configure --prefix=/usr/local
RUN make all
RUN make install

USER jenkins

WORKDIR /var/jenkins_home