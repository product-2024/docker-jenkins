# Jenkinsインストール手順

## 1. Jenkinsインストール

```bash
[root@localhost docker-jenkins]# sh jenkins-install.sh
bb077eef2bb1: Loading layer [==================================================>]  196.1MB/196.1MB
0f7f99d73f7c: Loading layer [==================================================>]  59.08MB/59.08MB
adcced563f50: Loading layer [==================================================>]  16.38kB/16.38kB
f4b4ba760ac7: Loading layer [==================================================>]  3.584kB/3.584kB
b1379b1206c9: Loading layer [==================================================>]  9.728kB/9.728kB
52ad5001ca77: Loading layer [==================================================>]  873.5kB/873.5kB
eac3ad5ffb06: Loading layer [==================================================>]   86.5MB/86.5MB
17c754bcd12c: Loading layer [==================================================>]  12.29kB/12.29kB
d9de19185a5b: Loading layer [==================================================>]  6.851MB/6.851MB
bfe06b1517d3: Loading layer [==================================================>]  102.8MB/102.8MB
e93fb479825b: Loading layer [==================================================>]  9.728kB/9.728kB
2247c1debd05: Loading layer [==================================================>]  5.632kB/5.632kB
704547c07568: Loading layer [==================================================>]  3.072kB/3.072kB
fee3ad5ac66e: Loading layer [==================================================>]  3.584kB/3.584kB
14d7fed16889: Loading layer [==================================================>]  1.799MB/1.799MB
341d69764bb4: Loading layer [==================================================>]  822.2MB/822.2MB
1ef3c7ef6d48: Loading layer [==================================================>]  42.07MB/42.07MB
a352c5a546ec: Loading layer [==================================================>]  3.584kB/3.584kB
965bd2cea2a2: Loading layer [==================================================>]  189.8MB/189.8MB
7d95ce2ffb78: Loading layer [==================================================>]  2.048kB/2.048kB
f1907fb20f21: Loading layer [==================================================>]  322.3MB/322.3MB
ffceb887792f: Loading layer [==================================================>]     11MB/11MB
72156f8b15f3: Loading layer [==================================================>]  2.048kB/2.048kB
a154444434f4: Loading layer [==================================================>]  4.608kB/4.608kB
c98028bae8d5: Loading layer [==================================================>]  8.704kB/8.704kB
a24224490250: Loading layer [==================================================>]  197.5MB/197.5MB
b0ea8b9d5768: Loading layer [==================================================>]  40.23MB/40.23MB
276806284a45: Loading layer [==================================================>]  68.77MB/68.77MB
f68d306f5456: Loading layer [==================================================>]  44.66MB/44.66MB
5f70bf18a086: Loading layer [==================================================>]  1.024kB/1.024kB
6face4f3f931: Loading layer [==================================================>]  539.1kB/539.1kB
48b1537315f0: Loading layer [==================================================>]  83.97kB/83.97kB
67fce39ffece: Loading layer [==================================================>]  291.6MB/291.6MB
637f324941db: Loading layer [==================================================>]  167.1MB/167.1MB
Loaded image: jenkins:2.440
[+] Running 2/2
 ✔ Network docker-jenkins_default  Created                                                    0.1s
 ✔ Container jenkins               Started                                                    0.0s
```

## 2. Jenkinsイメージロード確認

```bash
docker images
REPOSITORY         TAG           IMAGE ID       CREATED          SIZE
jenkins            2.440         caa3a356161f   16 minutes ago   2.62GB
```

## 3. アクセス

```bash
http://localhost:8080/jenkins
```

### 4. Jenkinsコンテナ停止

docker操作ユーザで以下コマンドを実行する。

```bash
docker compose down
```

## 補足

- dockerイメージビルド

```bash
docker build -t jenkins:2.440 .
```

- dockerイメージ保存

```bash
docker save jenkins:2.440 | gzip > jenkins-2.440.tar.gz
```

- コンテナ起動時ログ確認

```bash
docker logs -f jenkins
```

### Jenkinsアップデート

```bash
# docker cp jenkins.war jenkins:/usr/share/jenkins/.
# docker restart jenkins
```

参考情報として、`root`で`jenkins`コンテナにログインする場合は、
以下のコマンドを実行する。

```bash
# docker exec -it --user root jenkins /bin/bash
```

### Jenkinsプラグイン一覧

`Jenkins管理` → `スクリプトコンソール`

以下を入力し、`実行`するとプラグイン一覧が取得出来る。

```groovy
def pluginList = new ArrayList(Jenkins.instance.pluginManager.plugins)
pluginList.sort { it.getShortName() }.each{
  plugin ->
    println ("${plugin.getShortName()}:${plugin.getVersion()}")
}
```

### SSH公開鍵設定

クライアント側の公開鍵、秘密鍵を作成する。

```bash
bash-4.4# whoami
jenkins
bash-4.4# pwd
/
bash-4.4# ssh-keygen -t rsa
Generating public/private rsa key pair.
Enter file in which to save the key (/var/jenkins_home/.ssh/id_rsa):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /var/jenkins_home/.ssh/id_rsa.
Your public key has been saved in /var/jenkins_home/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:5F74PqoXvbmxZDfUqWRbwLp4LBC/T0voiT9C4cwki60 jenkins@228a3f4b0b56
The key's randomart image is:
+---[RSA 3072]----+
|                 |
|           .     |
|      . .   o    |
|    . o= . . o . |
|   o B..S.o + +  |
|  . o =o.B.= +   |
|   . .  *.@o=    |
|  E   .oo@+= .   |
|      o==o*o     |
+----[SHA256]-----+
bash-4.4# ls -rlta /var/jenkins_home/.ssh/
total 12
drwxrwxrwx. 1 jenkins jenkins 2496  4月 24 20:26 ..
-rw-r--r--. 1 jenkins jenkins  183  4月 24 20:26 known_hosts
-rw-r--r--. 1 jenkins jenkins  574  4月 24 21:40 id_rsa.pub
-rw-------. 1 jenkins jenkins 2610  4月 24 21:40 id_rsa
drwxr-xr-x. 1 jenkins jenkins  160  4月 24 21:40 .
bash-4.4# exit
exit
```

サンプルとして、`user01`に接続する。

```bash
# ll /home/
total 0
drwx------. 4 vagrant vagrant 111 Feb 20 16:38 vagrant/
drwx------. 4 user01  user01  104 Apr 24 09:57 user01/
```

以下のコマンドでdockerコンテナから公開鍵をコピーする。

```bash
# docker cp jenkins:/var/jenkins_home/.ssh/id_rsa.pub .
# ll
total 720972
-rw-r--r--. 1 vagrant vagrant     31718 Apr 24 11:10 README.md
-rwxr-xr-x. 1 vagrant vagrant       582 Apr 24 11:10 Dockerfile*
drwxr-xr-x. 1 vagrant vagrant       128 Apr 24 11:10 asset/
-rw-r--r--. 1 vagrant vagrant 735703913 Apr 24 11:10 jenkins_data.zip
-rw-r--r--. 1 vagrant vagrant       518 Apr 24 11:23 docker-compose.yml
drwxrwxrwx. 1 vagrant vagrant      2496 Apr 24 11:26 jenkins_data/
-rw-r--r--. 1 vagrant vagrant       574 Apr 24 12:40 id_rsa.pub
```

ホスト(`192.168.33.10`)側のユーザ`user01`になり、
以下の手続きでクライアントで作成した公開鍵を設定する。

```bash
[12:43:22 root@dev /vagrant/docker-jenkins-prototype]
# su - user01
[12:48:11 user01@dev ~]
# mkdir ~/.ssh
[12:48:27 user01@dev ~]
# ls -rlta
total 12
-rw-r--r--. 1 user01 user01 193 Sep  6  2017 .bash_profile
-rw-r--r--. 1 user01 user01  18 Sep  6  2017 .bash_logout
drwxr-xr-x. 4 root   root    35 Apr 24 09:33 ../
-rw-r--r--. 1 user01 user01 346 Apr 24 09:33 .bashrc
drwxr-xr-x. 2 user01 user01  40 Apr 24 09:57 .oracle_jre_usage/
drwxr-xr-x. 3 user01 user01  49 Apr 24 09:58 .gfclient/
drwxrwxr-x. 2 user01 user01   6 Apr 24 12:48 .ssh/
drwx------. 5 user01 user01 116 Apr 24 12:48 ./
[12:48:35 user01@dev ~]
# chmod 700 .ssh
[12:49:23 user01@dev ~]
# ls -rlta | grep .ssh
drwx------. 2 user01 user01   6 Apr 24 12:48 .ssh/
```

```bash
[12:50:36 root@dev ~]
# cat /vagrant/docker-jenkins-prototype/id_rsa.pub >> /home/user01/.ssh/authorized_keys
[12:51:20 root@dev ~]
# ll /home/user01/.ssh/authorized_keys
-rw-r--r--. 1 root root 574 Apr 24 12:51 /home/user01/.ssh/authorized_keys
[12:51:30 root@dev ~]
# chown user01:user01 /home/user01/.ssh/authorized_keys
[12:51:47 root@dev ~]
# chmod 600 /home/user01/.ssh/authorized_keys
[12:51:55 root@dev ~]
# ll /home/user01/.ssh/authorized_keys
-rw-------. 1 user01 user01 574 Apr 24 12:51 /home/user01/.ssh/authorized_keys
```

dockerコンテナからホスト(`192.168.33.10`)に接続確認

```bash
# docker exec -it jenkins /bin/bash
bash-4.4# ssh user01@192.168.33.10
Last login: Fri Apr 24 12:48:11 2020
[12:53:59 user01@dev ~]
# exit
logout
Connection to 192.168.33.10 closed.
bash-4.4$
```

### 接続先ユーザの登録

`jenkins`ユーザで作成した`user01`に関する秘密鍵を登録する。

![add_credentials](images/add_credentials.png)

認証情報の登録

![create_node](images/create_node.png)

jenkinsからssh接続後の`user01`配下のフォルダには、
以下のように自動でフォルダが作成される。

```bash
[13:13:09 user01@dev ~]
# cd jenkins_workspace/
[13:13:11 user01@dev ~/jenkins_workspace]
# ls -rlt
total 1488
-rw-rw-r--. 1 user01 user01 1522833  4月 24 13:12 remoting.jar
drwxrwxr-x. 4 user01 user01      34  4月 24 13:12 remoting/
```

### ツール設定

`Jenkinsの管理` → `Global Tool Configuration`で以下の設定をする。

- JDKのパス
- Gitのパス
- Mavenのパス

![setting_jdk_and_git](images/setting_jdk_and_git.png)

![setting_maven](images/setting_maven.png)

### その他

- `root`ユーザ以外で`docker`コマンドを実行させる場合

```bash
[13:57:32 root@dev ~]
# gpasswd -a user01 docker
Adding user user01 to group docker
[13:57:43 root@dev ~]
# cat /etc/group | grep docker
docker:x:993:user01
[13:57:52 root@dev ~]
# systemctl restart docker
```

`user01`で`docker`コマンドが問題なく使えることを確認した。

```bash
[13:59:36 user01@dev ~]
# docker ps -a
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                                              NAMES
fd2ab25f117f        jenkins:1.0         "/sbin/tini -- /usr/…"   3 hours ago         Up About a minute   0.0.0.0:8080->8080/tcp, 0.0.0.0:50000->50000/tcp   jenkins
```
